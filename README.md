# Url Shortner in Rails

To get it up and running:

Add /config/database.yml of and production settings if needed:

default: &default  
  adapter: sqlite3  
  pool: 5  
  timeout: 5000

development:  
  <<: *default  
  database: db/development.sqlite3


Add /config/application.yml to use gmail as mail server in dev and set a secret key:

GMAIL_USERNAME: ""  
GMAIL_PASSWORD: ""  
SECRET_KEY_BASE: "editsomesecretstuff"  


[*Test it live here*](http://fw.gg/)