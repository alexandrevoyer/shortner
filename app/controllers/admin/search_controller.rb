class Admin
  class SearchController < ApplicationController
    before_action :require_admin

    def results
      if params[:type] == 'email'
        if @users == User.find_by_email(params[:search]).nil?
          flash[:notice] = 'Email Not Found'
          redirect_to admin_search_path
        else
          @users = User.find_by_email(params[:search])
        end
      elsif params[:type] == 'id'
        if @users == User.find_by_id(params[:search]).nil?
          flash[:notice] = 'ID Not Found'
          redirect_to admin_search_path
        else
          @users = User.find_by_id(params[:search])
        end
      elsif params[:type] == 'short'
        if @links == Link.find_by_short(params[:search]).nil?
          flash[:notice] = 'Short URL Not Found'
          redirect_to admin_search_path
        else
          @links = Link.find_by_short(params[:search])
        end
      end
    end
  end
end
