class Admin
  class UsersController < ApplicationController
    before_action :require_admin

    def index
      @users = User.all
    end

    def show
      @users = User.find(params[:id]).links
    end

    def new
      @searchfor = User.find(params[:id])
    end
  end
end
