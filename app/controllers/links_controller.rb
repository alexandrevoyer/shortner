class LinksController < ApplicationController
  def new
    @shortened_url = Link.new
  end

  def create
    @shortened_url = Link.new(link_params)
    @shortened_url.user_id = current_user.id if user_signed_in?

    if @shortened_url.save
      flash[:shortened_short] = @shortened_url.short
      redirect_to new_link_url
    else
      render action: :new
    end
  end

  def show
    @shortened_url = Link.find_by_short(params[:short])
    if @shortened_url
      @shortened_url.update_attribute(:count, @shortened_url.count + 1)
      redirect_to @shortened_url.url
    else
      flash[:notice] = 'Link Not Found'
      redirect_to root_path
    end
  end

  def list
    if user_signed_in?
      @listall = current_user.links
    else
      redirect_to new_user_session_path
    end
  end

  def destroy
    Link.find(params[:id]).destroy
    flash[:notice] = 'Link Removed'
    redirect_to controller: 'links', action: 'list'
  end

  def update
    @shortened_url = Link.find(params[:id])
    respond_to do |format|
      if @shortened_url.update(link_params)
        format.html redirect_to(@shortened_url_url,
                                notice: 'Link was successfully updated.'
                               )
        format.json { respond_with_bip(@shortened_url) }
      else
        format.json { respond_with_bip(@shortened_url) }
      end
    end
  end

  private

  def link_params
    params.require(:link).permit(:url)
  end
end
