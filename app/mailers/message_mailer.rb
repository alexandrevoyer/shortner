class MessageMailer < ApplicationMailer

  default from: "FW.GG <noreply@fw.gg>"
  default to: "Your Name <steveh1981@gmail.com>"

  def new_message(message)
    @message = message
    
    mail subject: "Message from #{message.name}"
  end

end

