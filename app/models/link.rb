class Link < ActiveRecord::Base
  validates :url, presence: true
  validates :url, url: true
  before_create :randomize_short

  belongs_to :user

  private

  def randomize_short
    loop do
      self.short = SecureRandom.urlsafe_base64(4)
      break unless Link.where(short: short).exists?
    end
    # begin
    #	self.short = SecureRandom.urlsafe_base64(4)
    # end while Link.where(short: short).exists?
  end
end
